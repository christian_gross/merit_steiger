## Description
This Gitlab project is a script repository to showcase the implementation of the Steiger analysis in Chris Finan's MeRIT Python package (**MeRIT -  Mendelian Randomization for Identification of Targets**). https://gitlab.com/cfinan/merit

The Steiger Z tests are used to determine causal direction in Mendelian Randomisation analysis (exposure -> outcome / outcome -> exposure) as this can be unclear in cases of imprecisely measured exposure & outcome GWAS data. In layman's terms, the correlations between the genotype and the exposure, as well as genotype and outcome, are tested against each other with the assumption that the genotype should display a larger correlation with the **true** exposure than with the **true** outcome.

## Installation
Steiger is implemented as a module in the MeRIT Python package and as this cannot be installed independently. To request access to MeRIT and join the developer team, please get in touch with Chris Finan (c.finan@ucl.ac.uk). 

## Support
As a MeRIT developer, you can receive support by getting in touch with me via the drug_dev slack channel (drugdev-team.slack.com) or email me (c.gross@ucl.ac.uk). 

## Roadmap
1. Extensive testing will be implemented next, depending on the performance an alternative way of estimating genotype-exposure/outcome correlations will be implemented by first estimating variance explained from pvalues and sample size. 

2. Implementing a function which computes one-sample Steiger tests from commonly available GWAS summary data. 

## Contributing
Contributions are currently limited to the MeRIT developer team.

## Authors
Christian Groß

## Project status
Ongoing

