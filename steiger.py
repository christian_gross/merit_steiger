#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 31 13:52:39 2021

@author: christiangross

This module contains functions to compute the Steiger test of causality in case opf imprecisely
measured traits using GWAS summary data. It is mostly based on the following publication by
Hemani et al.
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5711033/
"""

import numpy as np
import pandas as pd
import scipy
from merit.utils.coloc_core import sd_pheno_approx
from merit.data_tools import calc_beta_from_p

#################################
#####Steiger anciallary functions
#################################
# TODO: write test
def compute_twosample_steiger_from_gwassummary(df,
                                               af_col = (None,None),
                                               pvalue_col = (None,None),
                                               pvalues_logged = (False,False),
                                               n_samples = (None,None)
                                               ):
    """
    This function is a wrapper to perform steiger z test of causality based on common GWAS
    summary data such as pvalues (genotype-exposure, genotype-outcome ) and number of samples.
    This function always performs two-sample Steiger tests, for one-sample steiger test please
    consider merit.steiger.compute_steiger() directly.

    Parameters
    ----------
    df : pandas.DataFrame
        Pandas DataFrame containing the exposure-,outcome GWAS p-values and if available the per
        variant number of samples.
    af_col : tuple of str
        A tuple containing the column name of the exposure GWAS allele frequencies at its first
        place and the column name of the outcome GWAS allele frequencies at its second. Failing to
        provide column names will cause a TypeError.
    pvalue_col : tuple of str
        A tuple containing the column name of the exposure GWAS pvalues at its first place and
        the column name of the outcome GWAS pvalues at its second. Failing to provide column names
        will cause a TypeError.
    pvalues_logged : tuple of bool, optional
        A tuple containing boolean True or False values indicating that the pvalue columns are
        -log10 transformed. First poisition of the tuple refers to the exposure pvalues, the second
        refers to the outcome pvalues. The default is (False,False).
    n_samples : tuple of str or positive number
        A tuple containing the column names of the number of sample columns of the exposure and
        outcoe at its first and second position respectively, alternatively it can contain
        positive numbers indicating the sample size for the exposure and outcome respectively.

    Raises
    ------
    TypeError
        In case that one of the parameters has incorrect type values or size, a TypeError is raised.

    Returns
    -------
    steiger_results : dict
        Dictionary containing the steiger pvalue (Steiger_P) and the steiger Z score (Steiger_Z)
        Steiger p-value. Null hypothesis is that both correlations are identical.
        Steiger Z-value, indicating support for the causal directionality.
        Z>0, X -> Y
        Z<0, Y -> X.
        Z=0, X independent Y.
    """
    #check if pvalues are known.
    if isinstance(pvalue_col,(tuple,list,np.ndarray)):
        #check if both pvalue_col values refer to columns in the dataset
        if np.all([i in df for i in pvalue_col]) and len(pvalue_col) == 2:
            pvalue_col_exp, pvalue_col_out = pvalue_col
        else:
            raise TypeError("The pvalue_col parameter must contain 2 values, containing the column"+
                            " names for the pvalue columns of the variant "+
                            "exposure and variant outcome sets respectively.")
    else:
        raise TypeError("The pvalue_col parameter must be either a tuple, list or numpy.array of "+
                        "length 2 but it is of type {}".format(type(pvalue_col)))
    #check if allele frequencies are known.
    if isinstance(af_col,(tuple,list,np.ndarray)):
        #check if both af_col values refer to columns in the dataset
        if np.all([i in df for i in af_col]) and len(af_col) == 2:
            af_col_exp, af_col_out = af_col
        else:
            raise TypeError("The af_col parameter must contain 2 values, containing the column"+
                            " names for the AF columns of the variant "+
                            "exposure and variant outcome sets respectively.")
    else:
        raise TypeError("The af_col parameter must be either a tuple, list or numpy.array of "+
                        "length 2 but it is of type {}".format(type(af_col)))
    #check if sample columns are known.
    if isinstance(n_samples,(tuple,list,np.ndarray)):
        #check if n_samples refer to columns in the dataset or are numbers
        if (np.all([i in df for i in n_samples]) or
            np.all([np.issubdtype(type(i),np.number) for i in n_samples])
            ) and len(n_samples) == 2:
            nsamples_exp, nsamples_out = n_samples
        else:
            raise TypeError("The n_samples parameter must contain 2 values, either containing the"+
                            " column names for the sample columns of the variant "+
                            "exposure and outcome sets respectively or or the number of samples.")
    else:
        raise TypeError("The n_samples parameter must be either a tuple, list or numpy.array of "+
                        "length 2 but it is of type {}".format(type(n_samples)))

    #check if pvalues_logged contains two boolean values.
    if isinstance(pvalues_logged,(tuple,list,np.ndarray)):
        #check if n_samples refer to columns in the dataset or are numbers
        if np.any([type(i) is not bool for i in pvalues_logged]) or len(pvalues_logged) != 2:
            raise TypeError("The pvalues_logged parameter must contain 2 boolean values")
    else:
        raise TypeError("The pvalues_logged parameter must be either a tuple, list or numpy.array "+
                        "of length 2 but it is of type {}".format(type(pvalues_logged)))

    #Infering beta and SE from p
    beta_se_gx = calc_beta_from_p(df.copy(),
                                allele_freq = af_col_exp,
                                pvalue = pvalue_col_exp,
                                pvalue_logged = pvalues_logged[0],
                                no_of_samples = nsamples_exp,
                                keep_cols = False)

    beta_se_gy = calc_beta_from_p(df.copy(),
                                allele_freq = af_col_out,
                                pvalue = pvalue_col_out,
                                pvalue_logged = pvalues_logged[1],
                                no_of_samples = nsamples_out,
                                keep_cols = False)

    #converting beta standard error to standard deviation
    #SD = np.sqrt(n)*SE
    beta_se_gx['standard_deviation'] = beta_se_gx['standard_error']*np.sqrt(nsamples_exp)

    #converting beta SE to SD and estimating the phenotype SD of exposure and outcome
    #SD = np.sqrt(n)*SE
    if isinstance(nsamples_exp, str):
        beta_se_gx['standard_deviation'] = beta_se_gx['standard_error']*np.sqrt(beta_se_gx[nsamples_exp])
        sd_x = sd_pheno_approx(variancebeta = beta_se_gx['standard_deviation']**2,
                                maf = beta_se_gx[af_col_exp],
                                n = beta_se_gx[nsamples_exp])
    else:
        beta_se_gx['standard_deviation'] = beta_se_gx['standard_error']*np.sqrt(nsamples_exp)
        sd_x = sd_pheno_approx(variancebeta = beta_se_gx['standard_deviation']**2,
                                maf = beta_se_gx[af_col_exp],
                                n = pd.Series([nsamples_exp]))
    if isinstance(nsamples_out, str):
        beta_se_gy['standard_deviation'] = beta_se_gy['standard_error']*np.sqrt(beta_se_gy[nsamples_exp])
        sd_y = sd_pheno_approx(variancebeta = beta_se_gy['standard_deviation']**2,
                                maf = beta_se_gy[af_col_out],
                                n = beta_se_gy[nsamples_out])
    else:
        beta_se_gy['standard_deviation'] = beta_se_gy['standard_error']*np.sqrt(nsamples_out)
        sd_y = sd_pheno_approx(variancebeta = beta_se_gy['standard_deviation']**2,
                                maf = beta_se_gy[af_col_out],
                                n = pd.Series([nsamples_out]))

    #computing standardized beta (correlation) and aggregating them to single value
    #ß = (s_g/s_y) * beta
    roh_gx = np.sqrt(
                    np.sum(
                        (
                            beta_se_gx['standard_deviation']/sd_x * beta_se_gx['effect_size']
                        )**2)
                    )
    roh_gy = np.sqrt(
                    np.sum(
                        (
                            beta_se_gy['standard_deviation']/sd_y * beta_se_gy['effect_size']
                        )**2)
                    )

    #computes steiger test.
    steiger_p, steiger_z = compute_steiger(roh_gx,
                                           roh_gy,
                                           np.mean(nsamples_exp),
                                           roh_xy = None,
                                           N2 = np.mean(nsamples_out),
                                           twotailed = True)

    steiger_results = {'Steiger_P':steiger_p,'Steiger_Z':steiger_z}
    return steiger_results

################################
#####Steiger test core functions
################################
# TODO: write test
def compute_steiger(roh_gx,
                    roh_gy,
                    N1,
                    roh_xy = None,
                    N2 = None,
                    twotailed = True):
    """
    This function runs the tests for one and two sample steiger tests, given correlation
    and sample number information.
    This method is based on the following publication:
    https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5711033/

    Parameters
    ----------
    roh_gx : float
        Sample correlation coefficient between genotypes and exposure variable.
    roh_gy : float
        Sample correlation coefficient between genotypes and outcome variable.
    N1 : int
        In case of two sample Steiger test, this parameter should contain the number of samples
        of the exposure GWAS. In case of single sample Steiger test, this parameter should
        contain the number of samples underlying the exposure and outcome GWAS.
    roh_xy : float,NoneType optional
        Optional, only needed for single sample steiger test. Sample correlation coefficient
        between exposure and outcome variable. The default is None.
    N2 : int,NoneType optional
        Optional, only needed for two sample steiger test. Number of samples of the outcome GWAS.
        The default is None.
    twotailed : bool, optional
        Indicates if two tailed p-value is computed or one-tailed. The default is True.

    Raises
    ------
    TypeError
        TypeError is raised if N2 and roh_xy have unexpected arguments because they are mutually
        exclusive.

    Returns
    -------
    steiger_p : float
        Steiger p-value. Null hypothesis is that both correlations are identical.
    steiger_z : float
        Steiger Z-value , indicating support for the causal directionality
        Z>0, X -> Y
        Z<0, Y -> X.
        Z=0, X independent Y.
    """

    if (roh_xy == None and N2 == None) or (roh_xy != None and N2 != None):
        raise TypeError('N2:{}, roh_xy:{}'.format(N2,roh_xy),"It is not clear if single or two "+
                        "sample steiger test should be performed. For single sample steiger test,"+
                        "roh_xy needs to be defined, for two sample steiger N2 needs to be defined"+
                        ". Defining none or both leads to this error.")
    Z_gx = fishers_z_transform(roh_gx)
    Z_gy = fishers_z_transform(roh_gy)

    if roh_xy != None and N2 == None:
        print("Do one sample steiger test for correlated correlations.")
        steiger_z = one_sample_steiger_test(Z_gx,Z_gy,N1,roh_gx,roh_gy,roh_xy)

    elif roh_xy == None and N2 != None:
        print("Do two sample steiger test for uncorrelated correlations.")
        steiger_z = two_sample_steiger_test(Z_gx,Z_gy,N1,N2)

    else:
        raise TypeError('roh_gx:{}, roh_gy:{}, N1:{}, roh_xy:{}, N2:{}, twotailed:{}'.format(
            roh_gx,roh_gy,N1,roh_xy,N2,twotailed),
                        "Something went wrong in the input, check the parameters."
                        )

    steiger_p = compute_p_from_Z(steiger_z,twotailed=twotailed)

    return steiger_p,steiger_z

# TODO: write test
def fishers_z_transform(roh):
    """
    This function takes the sample correlation coefficient of two variables and computes a Z score
    that is suitable for hypothesis testing. When the transformation is applied to the sample
    correlation coefficient, the sampling distribution of the resulting variable is approximately
    normal, with a variance that is stable over different values of the underlying true
    correlation.

    Parameters
    ----------
    roh : float
        Sample correlation coefficient between two variables.

    Returns
    -------
    Z : float
        Z-score of the sample correlation coefficient, usable for hypothesis testing.

    """
    Z = 1/2*np.log( (1+roh) / (1-roh) )
    return Z

# TODO: write test
def two_sample_steiger_test(Z_gx,
                            Z_gy,
                            N1,
                            N2):
    """
    This function performs the steiger Z test for two uncorrelated correlations.

    Parameters
    ----------
    Z_gx : float
        Z score of the correlation between genotype and exposure.
    Z_gy : float
        Z score of the correlation between genotype and outcome.
    N1 : int
        Number of samples in the exposure data set.
    N2 : int
        Number of samples in the outcome data set.

    Returns
    -------
    steiger_Z : float
        Steiger Z value, indicating support for the causal directionality
        Z>0, X -> Y
        Z<0, Y -> X.
        Z=0, X independent Y

    """
    steiger_Z = (Z_gx - Z_gy) / np.sqrt( (1/(N1-3)) + (1/(N2-3)) )
    return steiger_Z

# TODO: write test
def one_sample_steiger_test(Z_gx,
                            Z_gy,
                            N,
                            roh_gx,
                            roh_gy,
                            roh_xy):
    """
    This function performs the steiger Z test for two correlated correlations.

    Parameters
    ----------
    Z_gx : float
        Z score of the correlation between genotype and exposure.
    Z_gy : float
        Z score of the correlation between genotype and outcome.
    N : int
        Number of samples.
    roh_gx : float
        Sample correlation coefficient between genotypes and exposure variable.
    roh_gy : float
        Sample correlation coefficient between genotypes and outcome variable.
    roh_xy : float
        Sample correlation coefficient between exposure and outcome variables.

    Returns
    -------
    steiger_Z : float
        Steiger Z value, indicating support for the causal directionality
        Z>0, X -> Y
        Z<0, Y -> X.
        Z=0, X independent Y

    """
    rm2 = 1/2 * (roh_gx**2 + roh_gy**2)
    f = (1 - roh_xy) / (2*(1 - rm2))
    h = (1 - f*rm2) / (1 - rm2)
    steiger_Z = (Z_gx - Z_gy) * (np.sqrt(N-3) / np.sqrt(2*(1-roh_xy)*h))
    return steiger_Z

def compute_p_from_Z(Z_score,
                     twotailed = True):
    """
    This function takes a Z score and computes its pvalue with respect to a normal
    distribution. Can choose between one and two-tailed pvalue.

    Parameters
    ----------
    Z_score : float
        Z score of an investigated variable.
    twotailed : bool
        Indicates if one or two tailed pvalue is computed. default = True

    Returns
    -------
    p : float
        p value of the provided Z score.

    """
    p = 1 - scipy.stats.norm.cdf(abs(Z_score))
    if twotailed:
        p = p*2
    return p
